<p align="center">
	<img alt="logo" src="logo.png" width="400">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">我是readme.md 文件</h1>
<h5 align="center">专注研发xxx产品，专业提供xxxx解决方案</h5>

---

## 前言：️️
为了保证新同学不迷路，请允许我唠叨一下：无论您从何处看到本篇文章，最新开发文档永远在：[https://sa-token.cc](https://sa-token.cc)，
建议收藏在浏览器书签，如果您已经身处本网站下，则请忽略此条说明。

本文档将会尽力讲解每个功能的设计原因、应用场景，用心阅读文档，你学习到的将不止是 `Sa-Token` 框架本身，更是绝大多数场景下权限设计的最佳实践。


## Sa-Token 功能一览

Sa-Token 目前主要五大功能模块：登录认证、权限认证、单点登录、OAuth2.0、微服务鉴权。

- **登录认证** —— 单端登录、多端登录、同端互斥登录、七天内免登录
- **权限认证** —— 权限认证、角色认证、会话二级认证
- **更多功能正在集成中...** —— 如有您有好想法或者建议，欢迎加群交流

功能结构图：

![sa-token-js](https://color-test.oss-cn-qingdao.aliyuncs.com/sa-token/x/sa-token-js4.png 's-w')


## 开源仓库 Star 趋势

<p class="un-dec-a-pre"></p>

[![github-chart](https://starchart.cc/dromara/sa-token.svg 'GitHub')](https://starchart.cc/dromara/sa-token)

如果 Sa-Token 帮助到了您，希望您可以为其点上一个 `star`：
[码云](https://gitlab.com/kuczae/docs-branched)、
[GitHub](https://github.com/dromara/sa-token)

## 使用Sa-Token的开源项目 
参考：[Sa-Token 生态](/more/link)


## 交流群
加入 Sa-Token 框架 QQ、微信讨论群：[点击加入](/more/join-group.md)


